let point = []

function setup() {
  createCanvas(windowWidth, windowHeight)
  frameRate(6)

  for(i = 0; i < 1; i++) {
    point[i] = new Walker()
  }

  // point.vel.x = random(0, 10)
  // point.vel.y = random(0, 10)
}

function draw() {
  background(255);
  translate(width/2, height/2)
  for(i = 0; i < point.length; i++) {
    point[i].display()
    point[i].update()
    // print(point[i].objectName, point.length)
  }
}
